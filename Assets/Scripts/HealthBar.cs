﻿using UnityEngine; 
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {
 public GameMaster Master;
	public int[] curhealth;
	public int[] maxhealth;
	public Image[] bar;
	public float Fillarea;

	// Use this for initialization
	void Awake(){
		Master=GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster>();
		curhealth = new int[Master.Chealth.Length];
		maxhealth = new int[Master.Mhealth.Length];
		bar = new Image[2]; 
		curhealth = Master.Chealth;
		maxhealth = Master.Mhealth;
		bar[0] = this.GetComponent<Image> ();
		bar[1]= GameObject.Find("Mana Bar").GetComponent<Image>();
	}
	void Start () {
		Fillarea = curhealth [0] / maxhealth [0];
		bar[0].fillAmount = Fillarea;
	}
	
	// Update is called once per frame
	void Update () {
		Fillarea = (float)Master.Chealth[0] / (float)maxhealth [0];
		bar[0].fillAmount = Fillarea;

			if (Fillarea > 1)
				Fillarea = 1;
			if (Fillarea < 0.0f)
				Fillarea = 0;

	}
}
