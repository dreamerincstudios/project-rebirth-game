﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CorpsRaiderSqaudAI : MonoBehaviour {
	/* 3/22/2015 
	 * Create script that combines SquadEnemyAI and Raider Scripts into one master script. 
	 * Setup Stats for each member of the squad.
	 * Setup patrol points and patrol area
	 * WPG- Waypoint Guard WPP- Partol Waypoint WPCC- Commander points;
	 * 
	 */
	#region
	private static GameMaster master;
	public bool PatrolInOrder;
	public int AORange = 300; // Max patrol range further point from center a waypoint canbe places
	public List<GameObject> Raider; // Gameobject for Enemy Raider
	public List<GameObject> Specialist; //Gameobject for Enemy Specialist
	public List<GameObject> Captian;//Gameobject for Enemy Captian
	public List<GameObject> Waypoints; // 	points for enemies to patrol;
	public Transform CommandCenter;
	public int[] WaypointCNT;// How many waypoint in AORange. [0] = total [1]= Guard Stations [2]= Partol routes [3}= High Value Restricted Area
	public int Level;
	public List<NavMeshAgent> agent;
	List<SphereCollider> ViewArea;
	public List<GameObject> Routes;


	#endregion

	void Awake(){
		CommandCenter = this.transform;
		master=GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster>();
		WaypointCNT = new int[5];
	}
	// Use this for initialization
	void Start () {
		if (Raider.Count>0) 
			SetupRaider (master.diff);
		if (Specialist.Count>0) 
			SetupSpecialist (master.diff);
		if (Captian.Count>0) 
			SetupCaptian (master.diff);
		GetAOWaypoints ();
		GetNavMeshAgents ();

	}
	
	// Update is called once per frame
	void Update () {
	}
	#region
	void SetupRaider(int ModDiff){
		PlayerCharacter pc;
		foreach (GameObject go in Raider) {
			pc=go.GetComponent<PlayerCharacter>();
				pc.Name= "Raider";
			pc.Level=Level*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue = 10*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue = 14*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue = 13*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue = 12*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue = 12*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue = 17*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue = 20*ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue = 10*ModDiff;
			pc.GetVital ((int)VitalName.Health).BaseValue = 94;
			pc.GetVital ((int)VitalName.Mana).BaseValue = 20;
			pc.StatUpdate ();

		}
	}
	
	void SetupSpecialist(int ModDiff){
		PlayerCharacter pc;
		foreach (GameObject go in Specialist) {
			pc = go.GetComponent<PlayerCharacter> ();
			pc.Name = "Raider";
			pc.Level = Level * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue = 10 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue = 14 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue = 13 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue = 12 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue = 12 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue = 17 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue = 20 * ModDiff;
			pc.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue = 10 * ModDiff;
			pc.GetVital ((int)VitalName.Health).BaseValue = 94;
			pc.GetVital ((int)VitalName.Mana).BaseValue = 20;
			pc.StatUpdate ();
		}
	}
	
	void SetupCaptian(int ModDiff){
			PlayerCharacter pc;
			foreach (GameObject go in Captian) {
				pc=go.GetComponent<PlayerCharacter>();
				pc.Name= "Raider";
				pc.Level=Level*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue = 10*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue = 14*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue = 13*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue = 12*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue = 12*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue = 17*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue = 20*ModDiff;
				pc.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue = 10*ModDiff;
				pc.GetVital ((int)VitalName.Health).BaseValue = 94;
				pc.GetVital ((int)VitalName.Mana).BaseValue = 20;
				pc.StatUpdate ();
	}

}
	#endregion
	void GetAOWaypoints(){
		GameObject[] GOS = GameObject.FindGameObjectsWithTag ("WPG");
		GameObject[] GOS1 = GameObject.FindGameObjectsWithTag ("WPP");
		GameObject[] GOS2 = GameObject.FindGameObjectsWithTag ("WPCC");
		GameObject[] GOS3 = GameObject.FindGameObjectsWithTag ("Guard Route");
		// add Guard Waypoints to list
		foreach (GameObject go in GOS) {
			float dist = Vector3.Distance (CommandCenter.position,go.transform.position);
			if(dist<AORange){
			Waypoints.Add (go);
				WaypointCNT[0]++;
				WaypointCNT[1]++;
			}		}
		// add patrol points to list
		foreach (GameObject go in GOS1) {
			float dist = Vector3.Distance (CommandCenter.position,go.transform.position);
			if(dist<AORange && this.transform.parent == go.transform.parent){
				Waypoints.Add (go);
				WaypointCNT[0]++;
				WaypointCNT[2]++;
			}
		}
		//add protection waypoints to list
		foreach (GameObject go in GOS2) {
			float dist = Vector3.Distance (CommandCenter.position,go.transform.position);
			if(dist<AORange){
				Waypoints.Add (go);
				WaypointCNT[0]++;
				WaypointCNT[3]++;
			}
		}
		foreach (GameObject go in GOS3) {
			float dist = Vector3.Distance (CommandCenter.position,go.transform.position);
			if(dist<AORange){
				Routes.Add (go);
				WaypointCNT[0]++;
				WaypointCNT[4]++;
			}
		}
		}


		void GetNavMeshAgents(){
		foreach (GameObject go in Raider)
			agent.Add (go.GetComponent <NavMeshAgent>());
	}


}