﻿using UnityEngine;
using System.Collections;

public class HealthControl : MonoBehaviour {
	public PlayerCharacter PC;
	public int MaxHealth;
	public int CurHealth;
	public int MaxMana;
	public int CurMana;
	private float manatimer;
	// Use this for initialization
	void Start () {
		PC = this.GetComponent<PlayerCharacter> ();
		MaxHealth = PC.GetVital ((int)VitalName.Health).AdjustBaseValue;
		CurHealth = PC.GetVital ((int)VitalName.Health).AdjustBaseValue;
		MaxMana = PC.GetVital ((int)VitalName.Mana).AdjustBaseValue;
		CurMana=  PC.GetVital ((int)VitalName.Mana).AdjustBaseValue;
	}
	
	// Update is called once per frame
	void Update () {
		if (CurHealth == 0) {
			Destroy (this.gameObject);
		}
		if (CurMana < MaxMana && manatimer <= 0) {
			AdjustMana (2);
			int mt = PC.GetSkill ((int)SkillName.Mana_Recover).AdjustBaseValue;
			manatimer = 2.5f / (float)mt * 7;
		}
		if (manatimer > 0) {manatimer -= Time.deltaTime;}
		else{manatimer = 0;}
	}

	public void AdjustHealth(int adj){
		CurHealth += adj;
		
		if (CurHealth < 0) 
			CurHealth = 0;
		
		if (CurHealth > MaxHealth) 
			CurHealth = MaxHealth;	
	}
	public void AdjustMana(int adj){
		CurMana += adj;
		
		if (CurMana < 0) 
			CurMana = 0;
		
		if (CurMana > MaxMana) 
			CurMana = MaxMana;	
		}
}
