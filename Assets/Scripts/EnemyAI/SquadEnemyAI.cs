﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SquadEnemyAI : MonoBehaviour {
	public List<GameObject> EnemySquad;
	public List<GameObject> Waypoints;
	public NavMeshAgent agent;
	public SphereCollider col;
	private static System.Random random = new System.Random();
	public bool rotationFix = false;
	public float pauseAtDestinationDuration = 0.0f;
	private bool isPaused = false;
	private float pauseElapsed = 0.0f;
	private float testInterval = 1.0f;
	private float elapsedInterval = 0.0f;
	public bool PlayerInArea = false;
	public float fieldOfViewAngle = 110f;           // Number of degrees, centred on forward, for the enemy see.
	public Transform PlayerLocal;


	void Awake(){
		col = this.GetComponent<SphereCollider>();
		agent = this.GetComponent<NavMeshAgent> ();
	}
	// Use this for initialization
	void Start () {
		GameObject[] Gos=GameObject.FindGameObjectsWithTag("Enemy");
		foreach (GameObject go in Gos)
			if (this.transform.parent == go.transform.parent) {
				EnemySquad.Add (go);
			}
		GameObject[] GosW=GameObject.FindGameObjectsWithTag("Waypoint");
		foreach (GameObject go in GosW)
		if (this.transform.parent == go.transform.parent) {
			Waypoints.Add (go);
		}
		UpdateDestination ();
	}

	// Update is called once per frame
	void Update () {

		if (!PlayerInArea) {
			if (elapsedInterval > testInterval) {
				if (pauseAtDestinationDuration == 0) {
					UpdateDestination ();
				} else {
					isPaused = true;
				}
				testInterval = random.Next (1);
				elapsedInterval = 0.0f;
			}
			if (!isPaused) {
				elapsedInterval += Time.deltaTime;
			} else {
				pauseElapsed += Time.deltaTime;
				if (pauseElapsed > pauseAtDestinationDuration) {
					pauseElapsed = 0.0f;
					isPaused = false;
					UpdateDestination ();
				}
			}
	
		}
	}

	private void UpdateDestination()
	{

		int nextDestination = random.Next(Waypoints.Count-1);
		
		if (agent.remainingDistance < 1)
		{            
			agent.destination = Waypoints[nextDestination].transform.position;
			if (rotationFix)
			{
				transform.LookAt(Waypoints[nextDestination].transform);
				transform.Rotate(Vector3.up, 90);
				agent.updateRotation = false;
			}
		}
		
	}

	void OnTriggerStay(Collider other){
	//if (other.gameObject.tag == "Player")
		//	PlayerInArea = false;
		
		// Create a vector from the enemy to the player and store the angle between it and forward.
		Vector3 direction = other.transform.position - transform.position;
		float angle = Vector3.Angle(direction, transform.forward);
		
		// If the angle between forward and where the player is, is less than half the angle of view...
		if (angle < fieldOfViewAngle * 0.5f) {
			RaycastHit hit;
			
			// ... and if a raycast towards the player hits something...
			if (Physics.Raycast (transform.position + transform.up, direction.normalized, out hit, col.radius)) {
				// ... and if the raycast hits the player...
				if (hit.collider.gameObject.tag == "Player") {
					// ... the player is in sight.
					PlayerInArea = true;
					PlayerLocal=other.transform;
					Invoke("CallForBackup",10);
				}
			}

		}
	}
	void OnTriggerExit(Collider other){
		PlayerInArea = false;
		agent.stoppingDistance = 0;
		Invoke("UpdateDestination",8.5f);
	}

	void CallForBackup(){
		GameObject[] Gos = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject go in Gos)
			if (this.transform.parent == go.transform.parent) {
			go.GetComponent<SquadEnemyAI>().PlayerInArea= true;
			go.GetComponent<SquadEnemyAI>().PlayerLocal=PlayerLocal;


		}
	}
}
