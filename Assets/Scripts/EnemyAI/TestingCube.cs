﻿using UnityEngine;
using System.Collections;

public class TestingCube : MonoBehaviour {
	public int Level;
	// Use this for initialization
	void Start () {
		SetupRaider (GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster> ().diff );
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void SetupRaider(int ModDiff){
		PlayerCharacter pc = this.GetComponent<PlayerCharacter> ();
		pc.Name = "TestCube";
		pc.Level = (int)Level *(int) ModDiff;
		pc.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12 +Level*3;
		pc.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue = 10 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue = 14 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue = 13 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue = 12 +Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue = 12 +Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue = 17 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue = 20 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue = 10 + Level;
		pc.GetVital ((int)VitalName.Health).BaseValue = 94;
		pc.GetVital ((int)VitalName.Mana).BaseValue = 20*(int)ModDiff;
		pc.StatUpdate ();;
	}
}
