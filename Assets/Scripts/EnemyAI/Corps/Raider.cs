﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections; 
namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof (NavMeshAgent))]
public class Raider : MonoBehaviour {
	private GameObject player;
	public Transform[] path;
	private FSMSystem fsm;
	public int Level;
	public float curHealth;
	private NavMeshAgent agent;
		private ThirdPersonCharacter character ; // the character we are controlling
	public void SetTransition(Transition t) { fsm.PerformTransition(t); }
	

	void Awake(){
		agent= this.GetComponent<NavMeshAgent>();
			character = this.GetComponent<ThirdPersonCharacter>();
			agent.updateRotation = false;
			agent.updatePosition = true;

			//character=this.GetComponent<ThirdPersonCharacter>();
	}
	// Use this for initialization
	void Start () {
		MakeFSM();
		player = GameObject.FindGameObjectWithTag("Player");
		SetupRaider (GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster> ().diff );

	}
	public void FixedUpdate()
	{
		fsm.CurrentState.Reason(player, this.gameObject);
		fsm.CurrentState.Act(player, this.gameObject);
	}
	// Update is called once per frame
	void Update () {
	
	}
	
	/* The NPC has two states: FollowPath, ChasePlayer
	 * If it's on the first state and SawPlayer transition is fired, it changes to ChasePlayer
	 * If it's on ChasePlayerState and LostPlayer transition is fired, it returns to FollowPath
	 * 
	 */
	private void MakeFSM()
	{
		FollowPathState Follow = new FollowPathState (path, agent, character);
		Follow.AddTransition (Transition.SawPlayer, StateID.ChasingPlayer);
			
		ChasePlayerState Chase = new ChasePlayerState(agent,character);
		Chase.AddTransition(Transition.LostPlayer, StateID.FollowingPath);
			Chase.AddTransition (Transition.MeleeAtk, StateID.MeleeAtkPlayer);

			MeleeATKState Melee = new MeleeATKState (agent,character);
			Melee.AddTransition (Transition.SawPlayer, StateID.ChasingPlayer);
		fsm = new FSMSystem();
		fsm.AddState(Follow);
		fsm.AddState(Chase);

			fsm.AddState (Melee);
	}
	void SetupRaider(int ModDiff){
		PlayerCharacter pc = this.GetComponent<PlayerCharacter> ();
		pc.Name = "Raider";
		pc.Level = (int)Level *(int) ModDiff;
		pc.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12 +Level*3;
		pc.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue = 10 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue = 14 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue = 13 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue = 12 +Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue = 12 +Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue = 17 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue = 20 + Level;
		pc.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue = 10 + Level;
		pc.GetVital ((int)VitalName.Health).BaseValue = 94;
		pc.GetVital ((int)VitalName.Mana).BaseValue = 20*(int)ModDiff;
		pc.StatUpdate ();
		curHealth = pc.GetVital ((int)VitalName.Health).AdjustBaseValue;
	}

}
	public class FollowPathState : FSMState
	{
		private int currentWayPoint;
		private Transform[] waypoints;
		private NavMeshAgent agent;
		private ThirdPersonCharacter character;
		public FollowPathState(Transform[] wp, NavMeshAgent ag, ThirdPersonCharacter chr) 
		{ 
			waypoints = wp;
			currentWayPoint = 0;
			agent = ag;
			character=chr; 
			stateID = StateID.FollowingPath;
		}
		
		public override void Reason(GameObject player, GameObject npc)
		{
			// If the Player passes less than 15 meters away in front of the NPC
			int angle = 60;
			Vector3 here = npc.transform.position; // get player/weapon position...
			Vector3 forward = npc.transform.forward; // and forward direction
			// get colliders inside range:
			Collider[] cols = Physics.OverlapSphere(here, 20);
			foreach (Collider col in cols) { // check them all
				//Debug.Log (col.name);
				if (col && col.gameObject.tag=="Player") { // if it's an enemy...
					Vector3 dir = col.transform.position - here; // find enemy direction
					if (Vector3.Angle (dir, forward) <= angle / 2) { // if inside the angle...
						Debug.Log ("Chase Player");
					npc.GetComponent<Raider>().SetTransition(Transition.SawPlayer);
					}
				}		}
		}

		
		public override void Act(GameObject player, GameObject npc)
		{
			// Follow the path of waypoints
			// Find the direction of the current way point 
			//Vector3 vel = npc.GetComponent<Rigidbody>().velocity;
			//Vector3 moveDir = waypoints[currentWayPoint].position - npc.transform.position;
			agent.SetDestination(waypoints[currentWayPoint].position);
			character.Move (agent.desiredVelocity, false, false);
			agent.stoppingDistance = 0.0f;
			agent.speed = .45f;
			if (agent.remainingDistance < .25)
			{
				currentWayPoint++;
				if (currentWayPoint >= waypoints.Length)
				{
					currentWayPoint = 0;
				}
			}
			/*else
			{
				vel = moveDir.normalized * 10;
				
				// Rotate towards the waypoint
				npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation,
				                                          Quaternion.LookRotation(moveDir),
				                                          5 * Time.deltaTime);
				npc.transform.eulerAngles = new Vector3(0, npc.transform.eulerAngles.y, 0);
				
			}*/
			
			// Apply the Velocity
			//npc.GetComponent<Rigidbody>().velocity = vel;
		}
		
	} // FollowPathState
	
	public class ChasePlayerState : FSMState
	{private NavMeshAgent agent;
		private ThirdPersonCharacter character;
		private float Atktimer;
		public ChasePlayerState( NavMeshAgent ag, ThirdPersonCharacter chr)
		{	agent = ag;
			character=chr; 
			stateID = StateID.ChasingPlayer;
		
			Atktimer = 7.5f;
		}
		
		public override void Reason(GameObject player, GameObject npc)
		{ //Player gets out of range return to guard if guarding a post
//			if (guard && Vector3.Distance(npc.transform.position, player.transform.position) >= 30)
//				npc.GetComponent<Raider>().SetTransition(Transition.GuardHere);
//			// If NPC gets too far from guard post 
//			if (guard && Vector3.Distance(npc.transform.position, waypoints[0].position) >= 70)
//				npc.GetComponent<Raider>().SetTransition(Transition.GuardHere);

			// If the player has gone 30 meters away from the NPC, fire LostPlayer transition
			if (Vector3.Distance(npc.transform.position, player.transform.position) >= 30)
				npc.GetComponent<Raider>().SetTransition(Transition.LostPlayer);
			// once atk timer is good fire atk
			if (Atktimer <= 0.0f)
				npc.GetComponent<Raider> ().SetTransition (Transition.MeleeAtk);
		}
		
		public override void Act(GameObject player, GameObject npc)
		{
			agent.speed = .85f;
			agent.stoppingDistance = 5.5f;
			agent.SetDestination (player.transform.position);
			character.Move (agent.desiredVelocity, false, false);
			if (agent.remainingDistance <= .75)
				Atktimer -= Time.deltaTime;
		}
}
	/*public class GuardPostState: FSMState
	{private NavMeshAgent agent;
		private Transform[] waypoints;
		private ThirdPersonCharacter Character;
		public GuardPostState(Transform[] wp,NavMeshAgent ag,ThirdPersonCharacter chr){
			agent = ag;
			waypoints = wp;
			Character=chr;
			stateID=StateID.Guarding;
		}
		public override void  Reason(GameObject player, GameObject npc){
			// If the Player passes less than 15 meters away in front of the NPC
			int angle = 60;
			Vector3 here = npc.transform.position; // get player/weapon position...
			Vector3 forward = npc.transform.forward; // and forward direction
			// get colliders inside range:
			Collider[] cols = Physics.OverlapSphere(here, 20);
			foreach (Collider col in cols) { // check them all
				//Debug.Log (col.name);
				if (col && col.gameObject.tag=="Player") { // if it's an enemy...
					Vector3 dir = col.transform.position - here; // find enemy direction
					if (Vector3.Angle (dir, forward) <= angle / 2) { // if inside the angle...
						Debug.Log ("Chase Player");
						npc.GetComponent<Raider>().SetTransition(Transition.SawPlayer);
					}
				}		}
		}
		public override void Act (GameObject player, GameObject npc)
		{
			agent.SetDestination (waypoints [0].position);
			Character.Move (agent.desiredVelocity, false, false);
			agent.stoppingDistance = 0;
			npc.transform.LookAt (waypoints [1].position);

		}
	}*/
	public class MeleeATKState:FSMState{
		private NavMeshAgent agent;
		private ThirdPersonCharacter Character;
		private bool atked;
		public MeleeATKState(NavMeshAgent Ag,ThirdPersonCharacter chr){
			agent = Ag;
			Character = chr;
			stateID = StateID.MeleeAtkPlayer;
			atked = false;
		}
		public override void Reason (GameObject player, GameObject npc)
		{ if(atked)
			npc.GetComponent<Raider>().SetTransition(Transition.SawPlayer);
		}
		public override void Act (GameObject player, GameObject npc)
		{agent.stoppingDistance = .5f;
			agent.SetDestination (npc.transform.position);
			Character.Move (agent.desiredVelocity, false, false);
			if (agent.remainingDistance == 0) {
				Debug.Log("Attacking player");
				atked=true;
			}
		}
	}
}
