﻿using UnityEngine;
using System;

namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof (NavMeshAgent))]
public class AIMove : MonoBehaviour {
		
		public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
		public ThirdPersonCharacter character { get; private set; } // the character we are controlling

	// Use this for initialization
	void Start () {
			
			// get the components on the object we need ( should not be null due to require component so no need to check )
			agent = GetComponent<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();

	}
	
	// Update is called once per frame
	void Update () {
			
			character.Move(agent.desiredVelocity, false, false);
	
	}
}
}
