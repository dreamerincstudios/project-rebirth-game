﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Tester : MonoBehaviour {
	public GameObject player;
	public GameObject[] EnemyList;
	public Collider[] cols;
	public GameObject[] test;
	public int where;
	public float radian;
	public Vector3 epos;
	// Use this for initialization
	void Start () {
		where = 1;
		player = GameObject.FindGameObjectWithTag("Player");
	
	}
	
	// Update is called once per frame
	void Update () {
		int EnemyCnt = 0;
		//GameObject[] EnemyList;
		cols = Physics.OverlapSphere (player.transform.position, 15);
		foreach (Collider col in cols) {
			if (col && col.gameObject.tag == "Enemy") {
				EnemyCnt++;
			}
		}
		int cnt = 0;
		EnemyList = new GameObject[EnemyCnt];
		for (int i=0; i<cols.Length; i++) {
			if (cols [i].gameObject.tag == "Enemy") {
				EnemyList [cnt] = cols [i].gameObject;
				cnt++;
			}
			if (cnt > EnemyCnt)
				break;
		}/*
		test = new GameObject[cols.Length];
		for (int i =0; i<cols.Length; i++) {
			if (cols [i].gameObject.tag == "Enemy") {
				EnemyList [cnt] = cols [i].gameObject;
				cnt++;
			}
		}*/
		for (int i=0; i<EnemyList.Length; i++) {
		if(this.name==EnemyList[i].name)
				where =i;
			radian=where*Mathf.PI/(EnemyCnt/2);
		}
		epos = new Vector3 (8 * Mathf.Cos (radian) + this.transform.position.x,
		                    this.transform.position.y,
		                    8 * Mathf.Sin (radian) + this.transform.position.z);
	}
}
