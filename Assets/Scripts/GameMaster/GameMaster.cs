﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameMaster : MonoBehaviour {
	public static GameMaster Master;

	public int[] Mhealth; //Max Health 
	public int[] Chealth; //Current Health
	public int[] MMana; // Max Mana/Energy
	public int[] CMana ;// Current Mana/Energy
	public int diff=1;
	public int[] Exp;
	public int[] Level;
	public int Gil ;
	public string PCname;
	public int modelchoice;
	public int JumpAbility; // 1. Double Jump 2. Launch 
	public int SaveSlots;


	// Use this for initialization
	void Awake() {
		if (Master == null) {
			DontDestroyOnLoad (gameObject);
			Master=this;
		}
		else if(Master != this){
			Destroy (gameObject);
		}
		JumpAbility = 1;
		Level=new int[5] ;
		Mhealth = new int[5];
		MMana = new int[5] ;
		Chealth = new int[5];
		CMana = new int[5] ;
	}
	void start(){

		JumpAbility = 1;

	}
	// Update is called once per frame
	void Update () {
	
	}
	public void SaveGame(int savenum){
		BinaryFormatter bf= new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/RoPInfo"+savenum+".dat");
		RoPData data= new RoPData();
		data.Chealth=Chealth; data.level = Level; data.PCname=PCname; data.modelchoice=modelchoice;
		data.CMana = CMana; data.diff=diff; data.Gil = Gil;
		data.GameLevel = Application.loadedLevel;
		data.SaveTime = System.DateTime.Now.ToString ("MM/dd/yyyy HH:mm:ss");
		bf.Serialize (file, data); file.Close ();
	}
	public void loadGame(){
		if (File.Exists (Application.persistentDataPath + "/RoPInfo.dat")) {
			BinaryFormatter bf= new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + "/RoPInfo.dat", FileMode.Open);
			RoPData data= (RoPData)bf.Deserialize(file);
			file.Close ();
			Chealth=data.Chealth;data.level=Level;PCname=data.PCname;modelchoice=data.modelchoice;
			CMana=data.CMana;diff=data.diff;Gil=data.Gil;
			Application.LoadLevel(data.GameLevel);
		}}
	public void SaveList(){
		BinaryFormatter bf= new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/SaveList.dat");
		RoPSave data = new RoPSave ();
		data.NumberOfSaves = SaveSlots;
		bf.Serialize (file, data);file.Close ();
		}
	public void LoadList(){
		if (File.Exists (Application.persistentDataPath + "/SaveList.dat")) {
			BinaryFormatter bf= new BinaryFormatter();
			FileStream file = File.Open (Application.persistentDataPath + "/SaveList.dat", FileMode.Open);
			RoPSave data = (RoPSave)bf.Deserialize(file);
				file.Close(); 
			SaveSlots=data.NumberOfSaves;
			Application.LoadLevel(3);
			

	}
	}
}
[Serializable]
class RoPData
{	public int[] level;
	public int Gil ;
	public string PCname;
	public int modelchoice;
	public int[] CMana ;// Current Mana/Energy
	public int[] Chealth; //Current Health
	public int diff=1;
	public int GameLevel;
	public String SaveTime;
}
[Serializable]
class RoPSave{
	public int NumberOfSaves;
}