﻿using UnityEngine;
using System.Collections;

public class LoaderSPL : MonoBehaviour {
	public static LoaderSPL Loader;
	void Awake() {
		if (Loader == null) {
			DontDestroyOnLoad (gameObject);
			Loader = this;
		} else if (Loader != this) {
			Destroy (gameObject);
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
