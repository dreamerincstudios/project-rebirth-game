﻿using UnityEngine;
using System.Collections;

public class Elle : MonoBehaviour {
	public PlayerCharacter PC;
	public static GameMaster Master;
	public HealthBar healthbarsys;
	public float manatimer;

	void Awake () {
		Master = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster>();
		PC = this.GetComponent<PlayerCharacter> ();
	}
	// Use this for initialization
	void Start () {
		Setup ();
	}
	
	// Update is called once per frame
	void Update () {

if (Master.CMana [0] < Master.MMana [0] && manatimer <= 0) {
			AdjustMana (2);
			int mt=PC.GetSkill((int)SkillName.Mana_Recover).AdjustBaseValue	;
			manatimer=2.5f/(float)mt*7;
		}

		if(manatimer>0 )
			manatimer -= Time.deltaTime;
		if (manatimer < 0)
			manatimer = 0;
	}
	void Setup(){
		//PC.Name = "Elle";
		PC.Level = Master.Level[0];
		PC.GetPrimaryAttribute ((int)AttributeName.Vitality).BaseValue = 12;
		PC.GetPrimaryAttribute ((int)AttributeName.WillPower).BaseValue = 11;
		PC.GetPrimaryAttribute ((int)AttributeName.Concentration).BaseValue =10;
		PC.GetPrimaryAttribute ((int)AttributeName.Strength).BaseValue =14;
		PC.GetPrimaryAttribute ((int)AttributeName.Charisma).BaseValue =13;
		PC.GetPrimaryAttribute ((int)AttributeName.Luck).BaseValue =12;
		PC.GetPrimaryAttribute ((int)AttributeName.Awareness).BaseValue =12;
		PC.GetPrimaryAttribute ((int)AttributeName.Speed).BaseValue =17;
		PC.GetPrimaryAttribute ((int)AttributeName.Skill).BaseValue =20;
		PC.GetPrimaryAttribute ((int)AttributeName.Resistance).BaseValue =10;
		PC.GetVital ((int)VitalName.Health).BaseValue = 94;
		PC.GetVital ((int)VitalName.Mana).BaseValue = 40;
		PC.StatUpdate ();
		Master.Mhealth [0] = PC.GetVital ((int)VitalName.Health).AdjustBaseValue;
		Master.MMana [0] = PC.GetVital ((int)VitalName.Mana).AdjustBaseValue;
		Master.CMana[0] = Master.MMana[0];
		Master.Chealth[0] = Master.Mhealth[0];
	}

	public void AdjustHealth(int adj){
		int curHealth = Master.Chealth[0];
		int maxHealth = Master.Mhealth[0];
		curHealth += adj;
		
		if (curHealth < 0) 
			curHealth = 0;
		
		if (curHealth > maxHealth) 
			curHealth = maxHealth;	
		
		if(curHealth < 1)
			curHealth = 1;
		Master.Chealth[0] = curHealth;
		healthbarsys.bar[0].fillAmount= (float)curHealth / (float)maxHealth;
	}

	public void AdjustMana(int adj){

		int curMana = Master.CMana[0];
		int maxMana = Master.MMana[0];
			curMana += adj;
		
		if (curMana < 0) 
			curMana = 0;
		
		if (curMana > maxMana) 
			curMana = maxMana;	
		
		if(curMana < 1)
			curMana = 1;
		Master.CMana[0] = curMana;
		healthbarsys.bar [1].fillAmount = (float)curMana / (float)maxMana;
	}
}
