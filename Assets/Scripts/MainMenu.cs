﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour {
	public Animator anim;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if (Input.GetButtonUp ("Cancel")) {
			anim.SetTrigger ("Cancel");
		}
	}
	public void loadScene(int levelnum){
		Application.LoadLevel (levelnum);
	}
	public void CharacterChoice(int choice){
		GameMaster GM = GameObject.FindGameObjectWithTag ("GM").GetComponent<GameMaster> ();
		GM.modelchoice = choice;
		if (choice == 0) GM.PCname = "Riley";
		if (choice == 1) GM.PCname = "Elle";
		if (choice == 2) GM.PCname = "Maia";
		GM.Gil = 1000;
		GM.Level [0] = 3;
		GM.diff = 3;
	}
}
