﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadAsync : MonoBehaviour {
	public GameObject Loader;
	public int NumberOfQuads;
	public int Indexer; //First level to be dynamically loaded in building setting
	public List<GameObject> Loaders;

	// Use this for initialization
	void Start () {
		SetupLoadArea (NumberOfQuads);
	}
	
	// Update is called once per frame
	void Update () {

	}
	void SetupLoadArea(int number){
		GameObject gm = GameObject.FindGameObjectWithTag ("GM");
		int quads = (int)Mathf.Sqrt (NumberOfQuads);
		int di = (int)number / (int)quads;
		int cnt = 1;
		int cnt2 = 1;
		int IndexPasser = Indexer;
		if (number > 0) {
			for (int k=0; k<quads; k++) {
				for (int i=0; i<di; i++) {
					GameObject go =Instantiate (Loader, new Vector3 (600+1200 * i, 150, 600+1200 * k),
					                            Quaternion.identity)as GameObject;
					go.transform.parent=gm.transform; go.name="Quad "+ cnt +" Loader";
					Loaders.Add (go);
					cnt++;
				}
			}
			foreach(GameObject go1 in Loaders){
				go1.GetComponent<LoadDyn>().index=IndexPasser;
				go1.GetComponent<LoadDyn>().PieceNumber=cnt2;
				IndexPasser++; cnt2++;}
		
		}
	}
}
