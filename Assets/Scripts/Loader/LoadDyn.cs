﻿using UnityEngine;
using System.Collections;

public class LoadDyn : MonoBehaviour {
	public int index;
	public int PieceNumber;
	public GameObject quad;

	void Awake(){
		index=new int();
	}
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	}
	void OnTriggerEnter(Collider other){
		if (other.tag == "Player" && Application.loadedLevel!=index) {
			//Application.LoadLevelAdditiveAsync (index);
			Debug.Log ("Player entered " + this.name);
			Debug.Log("Quad "+ PieceNumber);
			AsyncOperation async1 = Application.LoadLevelAdditiveAsync(index);
			LoadQuadCoroutine (async1);
		}
	}
	void OnTriggerExit(Collider other){
		if (other.tag == "Player")
			quad=GameObject.Find("Quad "+ PieceNumber);
		Destroy (quad);}
	IEnumerator LoadQuadCoroutine(AsyncOperation async){

		Debug.Log ("Starting Loading:   " + (async.progress * 100));
		async.allowSceneActivation = false;
	
		yield return async;
		Debug.Log ("Loading Done");
			yield return new WaitForSeconds(2);
		async.allowSceneActivation = true; 
	}

}
