public class Attribute : BaseStat{
	public Attribute(){
		ExpToLevel = 50;
		LevelModifier = 1.05f;
	}
}

public enum AttributeName{
	Strength,
	Vitality,
	Awareness,
	Speed,
	Skill,
	Resistance,
	Concentration,
	WillPower,
	Charisma,
	Luck
}
