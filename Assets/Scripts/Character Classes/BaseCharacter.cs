using UnityEngine;
using System.Collections;
using System;

public class BaseCharacter : MonoBehaviour {
	
	private string _name;
	private int _level;
	private uint _freeExp;
	private string _class;
	private Attribute[] _primaryAttribute;
	private Vital[] _vital;
	private Skill[] _skill;
	private Abilities[] _ability;
	public void Awake(){
		_name = string.Empty;
		_level = 0;
		_freeExp = 0;
		_class = string.Empty;
		_primaryAttribute = new Attribute[Enum.GetValues(typeof(AttributeName)).Length];
		_vital = new Vital[Enum.GetValues(typeof(VitalName)).Length];
		_skill = new Skill[Enum.GetValues(typeof(SkillName)).Length];
		_ability = new Abilities[Enum.GetValues (typeof(AbilityName)).Length];
		SetupPrimaryAttributes();
		SetupVitals();
		SetupSkills();
		SetupAbilities ();
	}
	
	public string Name{
		get{return _name;}
		set{_name = value;}
	}
	public string Class{
		get{return _class;}
		set{_class = value;}
	}
	public int Level{
		get{return _level;}
		set{_level = value;}
	}
	
	public uint FreeExp{
		get{return _freeExp;}
		set{_freeExp = value;}
	}
	
	public void AddExp(uint exp){
		_freeExp += exp;
		CalculateLevle();
	}
	
	public void CalculateLevle(){
	}
	
	private void SetupPrimaryAttributes(){
		for(int cnt = 0; cnt < _primaryAttribute.Length; cnt++)
			_primaryAttribute[cnt] = new Attribute();
	}
	
	public Attribute GetPrimaryAttribute(int index){
		return _primaryAttribute[index];
	}
	
	private void SetupVitals(){
		for(int cnt = 0; cnt < _vital.Length; cnt++)
			_vital[cnt] = new Vital();
		SetupVitalModifiers();
	}
	private void SetupAbilities(){
		for(int cnt = 0; cnt < _ability.Length; cnt++)
			_ability[cnt] = new Abilities();
		SetupAbilitesModifiers();
	}
	
	public Vital GetVital(int index){
		return _vital[index];
	}
	public Abilities GetAbility(int index){
		return _ability[index];
	}
	
	private void SetupSkills(){
		for(int cnt = 0; cnt < _skill.Length; cnt++)
			_skill[cnt] = new Skill();
		SetupSkillModifiers();
	}
	
	public Skill GetSkill(int index){
		return _skill[index];
	}
	
	private void SetupVitalModifiers(){
		//health
		GetVital((int)VitalName.Health).AddModifier(
			new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Vitality),1.5f)
		);
		GetVital((int)VitalName.Health).AddModifier(
			new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Resistance),.33f)
			);

		//energy
		GetVital((int)VitalName.Energy).AddModifier(
			new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.WillPower),1)
		);
		
		//mana
		GetVital((int)VitalName.Mana).AddModifier(
			new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.WillPower),.5f)
		);
		GetVital((int)VitalName.Mana).AddModifier(
			new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration),.5f)
			);
	}
	private void SetupSkillModifiers(){		
		GetSkill((int)SkillName.Melee_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Strength),.33f));
		GetSkill((int)SkillName.Melee_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Skill),.33f));


		GetSkill((int)SkillName.Melee_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed),.33f));
		GetSkill((int)SkillName.Melee_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Resistance),.33f));
		GetSkill((int)SkillName.Melee_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Luck),.12f));

		GetSkill((int)SkillName.Magic_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration),.33f));
		GetSkill((int)SkillName.Magic_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.WillPower),.33f));
		GetSkill((int)SkillName.Magic_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Charisma),.33f));

		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Strength),.2f));
		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Charisma),.33f));
		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed),.10f));
		GetSkill((int)SkillName.Magic_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Charisma),.45f));

		GetSkill((int)SkillName.Ranged_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Concentration),.33f));
		GetSkill((int)SkillName.Ranged_Offence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed),.33f));
		
		GetSkill((int)SkillName.Ranged_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Speed),.33f));
		GetSkill((int)SkillName.Ranged_Defence).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Awareness),.33f));

		//Targeting and Motion detection
		GetSkill((int)SkillName.Range_Motion).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Awareness),.33f));
		GetSkill((int)SkillName.Range_Target).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Awareness),.33f));
	 // Status Changes IE Poison Confused Berzerk etc...
		
		GetSkill((int)SkillName.Status_Change).AddModifier(new ModifyingAttribute(GetPrimaryAttribute((int)AttributeName.Awareness),.33f));
		GetSkill((int)SkillName.Status_Change).AddModifier (new ModifyingAttribute (GetPrimaryAttribute ((int)AttributeName.Resistance), .33f));
	// Recovery Rates for Mana;
		
		GetSkill((int)SkillName.Mana_Recover).AddModifier (new ModifyingAttribute (GetPrimaryAttribute ((int)AttributeName.WillPower), .25f));
		GetSkill((int)SkillName.Mana_Recover).AddModifier (new ModifyingAttribute (GetPrimaryAttribute ((int)AttributeName.Concentration), .25f));
	}
	public void SetupAbilitesModifiers(){
		GetAbility ((int)AbilityName.Libra).AddModifier (new ModifyingAttribute (GetPrimaryAttribute ((int)AttributeName.Awareness), .25f));
	}
	
	public void StatUpdate(){
		for(int i = 0 ;i < _vital.Length; i++)
			_vital[i].Update();
		for(int j = 0; j< _skill.Length;j++)
			_skill[j].Update();
	}
}
