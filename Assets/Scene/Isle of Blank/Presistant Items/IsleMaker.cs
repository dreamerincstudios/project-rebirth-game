﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IsleMaker : MonoBehaviour {
	public List<Terrain> Pieces;
	// Use this for initialization
	void Start () {
		
		//Application.LoadLevelAdditiveAsync (1);
		//Application.LoadLevelAdditiveAsync (2);
		Application.LoadLevelAdditiveAsync (3);
		Application.LoadLevelAdditiveAsync (4);
	/*	Application.LoadLevelAdditiveAsync (5);
		Application.LoadLevelAdditiveAsync (6);
		Application.LoadLevelAdditiveAsync (7);
		Application.LoadLevelAdditiveAsync (8);
		Invoke("AddPiecestoList",5);
	*/}
	
	// Update is called once per frame
	void Update () {
	
	}
	void AddPiecestoList(){
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Terrain");
		foreach (GameObject go in gos) {
		Pieces.Add (go.GetComponent<Terrain> ());
		}
		Pieces.Sort (SortByNameAscending);
		SetNeighbors ();
	}
	void SetNeighbors(){
		Pieces[0].SetNeighbors(null,null,Pieces[11],Pieces[33]);
		Pieces[11].SetNeighbors(Pieces[0],null,Pieces[22],Pieces[34]);	
		Pieces[22].SetNeighbors(Pieces[11],null,Pieces[30],Pieces[35]);	
		Pieces[30].SetNeighbors(Pieces[22],null,Pieces[31],Pieces[1]);	
		Pieces[31].SetNeighbors(Pieces[30],null,Pieces[32],Pieces[2]);	
		Pieces[32].SetNeighbors(Pieces[31],null,null,Pieces[3]);	

		Pieces[33].SetNeighbors(null,Pieces[0],Pieces[34],Pieces[4]);
		Pieces[34].SetNeighbors(Pieces[33],Pieces[11],Pieces[35],Pieces[5]);	
		Pieces[35].SetNeighbors(Pieces[34],Pieces[22],Pieces[1],Pieces[6]);	
		Pieces[1].SetNeighbors(Pieces[35],Pieces[30],Pieces[2],Pieces[7]);	
		Pieces[2].SetNeighbors(Pieces[1],Pieces[31],Pieces[3],Pieces[8]);	
		Pieces[3].SetNeighbors(Pieces[2],Pieces[32],null,Pieces[9]);	

		
		Pieces[4].SetNeighbors(null,Pieces[33],Pieces[5],Pieces[10]);
		Pieces[5].SetNeighbors(Pieces[4],Pieces[34],Pieces[6],Pieces[12]);	
		Pieces[6].SetNeighbors(Pieces[5],Pieces[35],Pieces[7],Pieces[13]);	
		Pieces[7].SetNeighbors(Pieces[6],Pieces[1],Pieces[8],Pieces[14]);	
		Pieces[8].SetNeighbors(Pieces[7],Pieces[2],Pieces[9],Pieces[15]);	
		Pieces[9].SetNeighbors(Pieces[8],Pieces[3],null,Pieces[16]);
	
		Pieces[10].SetNeighbors(null,Pieces[4],Pieces[12],Pieces[17]);
		Pieces[12].SetNeighbors(Pieces[10],Pieces[5],Pieces[13],Pieces[18]);	
		Pieces[13].SetNeighbors(Pieces[12],Pieces[6],Pieces[14],Pieces[19]);	
		Pieces[14].SetNeighbors(Pieces[13],Pieces[7],Pieces[15],Pieces[20]);	
		Pieces[15].SetNeighbors(Pieces[14],Pieces[8],Pieces[16],Pieces[21]);	
		Pieces[16].SetNeighbors(Pieces[15],Pieces[9],null,Pieces[23]);
		
		Pieces[17].SetNeighbors(null,Pieces[10],Pieces[18],Pieces[24]);
		Pieces[18].SetNeighbors(Pieces[17],Pieces[11],Pieces[19],Pieces[25]);	
		Pieces[19].SetNeighbors(Pieces[18],Pieces[12],Pieces[20],Pieces[26]);	
		Pieces[20].SetNeighbors(Pieces[19],Pieces[14],Pieces[21],Pieces[27]);	
		Pieces[21].SetNeighbors(Pieces[20],Pieces[15],Pieces[23],Pieces[28]);
		Pieces[23].SetNeighbors(Pieces[21],Pieces[16],null,Pieces[29]);	

		Pieces[24].SetNeighbors(null,Pieces[17],Pieces[25],null);
		Pieces[25].SetNeighbors(Pieces[24],Pieces[18],Pieces[26],null);
		Pieces[26].SetNeighbors(Pieces[25],Pieces[19],Pieces[27],null);	
		Pieces[27].SetNeighbors(Pieces[26],Pieces[20],Pieces[28],null);	
		Pieces[28].SetNeighbors(Pieces[27],Pieces[21],Pieces[29],null);	
		Pieces[29].SetNeighbors(Pieces[28],Pieces[23],null,null);

		Pieces[23].SetNeighbors(Pieces[21],Pieces[15],null,Pieces[29]);


	}
	void SetUnseenPieceInactive(){
		Pieces [3].gameObject.SetActive (false);
		Pieces [24].gameObject.SetActive (false);
		Pieces [32].gameObject.SetActive (false);
	}
	private static int SortByName(Terrain o1, Terrain o2) {
		return o1.name.CompareTo(o2.name);
	}
	private static int SortByNameAscending(Terrain name1, Terrain name2)
	{

		return name1.name.CompareTo(name2.name);
	}
}
